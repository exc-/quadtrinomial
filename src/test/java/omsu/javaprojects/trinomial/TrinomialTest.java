package omsu.javaprojects.trinomial;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class TrinomialTest {
    @Test(expected = IllegalArgumentException.class)
    public void testDLowerThanZeroOrAIsZero(){
        QuadTrinomial qt = new QuadTrinomial(0,1,1);
        assertNull(qt.getRoots());
    }

    @Test
    public void testDEqualsZero(){
        QuadTrinomial qt = new QuadTrinomial(1,2,1);
        assertEquals(qt.getRoots()[0],-1, 10E-9);
        assertEquals(qt.getRoots()[1],-1,10E-9);
    }

    @Test
    public void testDBiggerThanZero(){
        QuadTrinomial qt = new QuadTrinomial(1,2,-3);
        assertEquals(qt.getRoots()[0],1, 10E-9);
        assertEquals(qt.getRoots()[1],-3,10E-9);
    }
}
